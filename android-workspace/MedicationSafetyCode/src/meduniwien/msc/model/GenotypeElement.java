/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package meduniwien.msc.model;

import java.util.ArrayList;

/**
 * This class represents a variant element. It could be an haplotype or a SNP variant.
 * 
 * @author Jose Antonio Mi�arro Gim�nez
 * */
public interface GenotypeElement {

	/**
	 * Get method that provides the genotype marker name.
	 * 
	 * @return		The name of the genotype marker.
	 * */
	public String getGeneticMarkerName();
	
	/**
	 * Get method that provides the name of the first variant in the combination.
	 * 
	 * @return		Name of the first variant that is related to the combination.
	 * */
	public String getVariant1();
	
	/**
	 * Get method that provides the name of the second variant in the combination.
	 * 
	 * @return		Name of the second variant that is related to the combination.
	 * */
	public String getVariant2();
	
	/**
	 * Updates the information regarding the combination of genotype variant.
	 * 
	 * @param variant1	Name of the first genotype variation in the combination.
	 * @param variant2	Name of the second genotype variation in the combination.
	 * */
	public void setVariants(String variant1, String variant2);
	
	/**
	 * Get method that provides the string that represents the combination of variants in alphabetical order.
	 * 
	 * @return		String of the variatians in alphabetical order.
	 * */
	public String getCriteriaSyntax();
	
	/**
	 * It clones the information of the instance into another instance.
	 * 
	 * @return	It produces an exactly copy of the instance.
	 * */
	public GenotypeElement clone();
	
	/**
	 * Get the list of URIs of the related classes in the ontology that corresponds to the Genotype variant.
	 * 
	 * @return It returns the list with a ontology class URIs related to the combination. The list can only contains 0, 1 or 2 URIs.
	 * */
	public ArrayList<String> getOntologyClassURIs();
}
