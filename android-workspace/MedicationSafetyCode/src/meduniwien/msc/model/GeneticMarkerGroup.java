/*
 Copyright (C) 2015  Matthias Samwald

 This software collection (genomic-cds) is available under a dual licensing
 whereby use of the software in projects that are licensed so as to be compatible
 with AGPL Version 3 may use the software under the terms of that license.
 See LICENSE.md or contact matthias.samwald@meduniwien.ac.at for further details
 on the dual licensing.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package meduniwien.msc.model;

import java.util.ArrayList;

import meduniwien.msc.exception.VariantDoesNotMatchAnyAllowedVariantException;

/**
 * 	This class represent a set of variants related to a haplotype or SNP element.
 * 
 * @author Jose Antonio Mi�arro Gim�nez
 * */
public interface GeneticMarkerGroup extends Comparable<GeneticMarkerGroup>{

	/**
	 * It provides the criteria syntax of a genetic marker combination in a particular position. The set of combinations is sorted by alphabetical order and includes the null;null at the position '0'.
	 * 
	 * @param position	The position of the combination in the set of 2-multicombination.
	 * @return	The textual description of the combination.
	 * */
	public String getGeneticMarkerVariantName(int position);
	
	/**
	 * It provides the position of one particular combination by its criteria syntax. The set of combinations is sorted by alphabetical order and includes the null;null at the position '0'.
	 * 
	 *  @param criteriaSyntax	The textual description of the combination.
	 *  @return		The position of the combination in the set of allele 2-multicombination.
	 * */	
	public int getPositionGeneticMarker(String criteriaSyntax);
	
	/**
	 * Get the genotype element (SNPs or Allele) from the position in the group.
	 * 
	 * @param position	The position of the combination in the set of 2-multicombination.
	 * @return The genotype element associated to the position in the group.
	 * @throws VariantDoesNotMatchAnAllowedVariantException 
	 * */
	public GenotypeElement getGenotypeElement(int position) throws VariantDoesNotMatchAnyAllowedVariantException;
	
	/**
	 * Get method that indicates the rank of the marker in a genotype.
	 * 
	 * @return	The rank of the combination set.
	 * */
	public int getRank();
	
	/**
	 * It indicates the number of combinations that can be formed with the group of SNPs.
	 * 
	 * @return		Number of 2-combinations with repetition from the group of SNP variations.
	 * */
	public int getNumberOfVariants();
	
	/**
	 * Get method that provides the id related to the combination set.
	 * 
	 * @return	The marker name related to the group of variants.
	 * */
	public String getGeneticMarkerName();
	
	/**
	 * Get list elements of the combination set.
	 * 
	 * @return	The list of elements that form this set.
	 * */
	public ArrayList<String> getListElements();
		
	
	/**
	 * Implements the compareTo method to sort the groups based on their rank number.
	 * 
	 * @param gmg	It represents an instance of Genetic_Marker_Group.
	 * @return		It returns a negative integer if its rank is lower than the rank of gmg, positive integer if its rank is greater than the rank of gmg, and 0 if the ranks are the same. 
	 * */
	public int compareTo(GeneticMarkerGroup gmg);
	
}
